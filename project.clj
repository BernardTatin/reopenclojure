(defproject OpenClojure "0.1.1-SNAPSHOT"
  :description "Is Clojure better than Lisp?"
  :url "https://bitbucket.org/BernardTatin/reopenclojure"
  :license {:name "MIT"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :source-paths ["src"]
  :main ^:skip-aot OpenClojure.core
  :target-path "target/%s"
  :uberjar-exclusions []
  :omit-source true
  :jar-name "reopen-clojure-%s.jar"
  :uberjar-name "reopen-clojure-%s-standalone.jar"
  :profiles {:uberjar {:aot :all}})
