(ns OpenClojure.core
  (:gen-class))

(defn mprint
  "Pretty print an object"
  [element]
  (print "   -> ")
  (println element))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!")
  (doall (map mprint args)))
